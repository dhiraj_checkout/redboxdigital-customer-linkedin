<?php

/** @var RedboxDigital_CustomerLinkedIn_Model_Resource_Setup  $installer */
$installer = $this;

$installer->startSetup();
$entity = $installer->getEntityTypeId('customer');
$attributeCode = 'linkedin_profile';
$this->removeAttribute($entity, $attributeCode);

if(!$installer->isAttributeExists($entity, $attributeCode)) {


    $attributeData = array(
        'type' => 'text',
        'label' => 'linkedIn',
        'input' => 'text',
        'visible' => true,
        'required' => false,
        'default_value' => '',
        'type' => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'length' => 250

    );

    $installer->addAttribute($entity, $attributeCode,$attributeData);

}

$installer->endSetup();

