<?php

/**
 * Resource setup class used for RedboxDigital_CustomerLinkedIn module
 * Class RedboxDigital_CustomerLinkedIn_Model_Resource_Setup
 * @package RedboxDigital
 * @author  Dhiraj Gangoosirdar <dhirajmetal@gmail.com>
 */
class RedboxDigital_CustomerLinkedIn_Model_Resource_Setup extends Mage_Customer_Model_Resource_Setup
{
    /**
     * Check if attribute exist
     * @param $entityTypeId
     * @param $attributeId
     * @return bool
     */

    public function isAttributeExists($entityTypeId, $attributeId)
    {
        try {

            $entityTypeId = $this->getEntityTypeId($entityTypeId);
            $attributeId = $this->getAttributeId($entityTypeId, $attributeId);
            return !empty($attributeId);

        } catch(Exception $e) {

            return false;
        }
    }
}