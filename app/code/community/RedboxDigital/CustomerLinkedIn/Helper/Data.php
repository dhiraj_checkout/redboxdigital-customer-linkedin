<?php

/**
 * Helper class
 * Class    RedboxDigital_CustomerLinkedIn_Helper_Data
 * @package RedboxDigital
 * @author  Dhiraj Gangoosirdar <dhirajmetal@gmail.com>
 */


class RedboxDigital_CustomerLinkedIn_Helper_Data extends Mage_Core_Helper_Abstract
{

    /**
     * Check if attribute is required
     * @param $attributeCode
     * @return mixed
     */

    public function isAttributeRequired($attributeCode)
    {
        /** @var $attribute Mage_Customer_Model_Attribute */
        $attribute = Mage::getSingleton('eav/config')->getAttribute('customer', $attributeCode);

        return $attribute->getIsRequired();
    }
}